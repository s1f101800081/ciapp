package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
    }

    @Test
    public void testRegisterUsernameSuccess1() throws ValidateFailedException{
        loginManager.register("superCoolAccount13", "Password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterUsernameFail1() throws ValidateFailedException{
        loginManager.register("supercoolaccount13", "Password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterUsernameFail2() throws ValidateFailedException{
        loginManager.register("sAs", "Password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterUsernameFail3() throws ValidateFailedException{
        loginManager.register("s23", "Password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterUsernameFail4() throws ValidateFailedException{
        loginManager.register("214982", "Password");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("Iniad", "Password");
    }
}